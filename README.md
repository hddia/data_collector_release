# data_collector

Version: 1.0.3

License: Apache 2.0

# Supported features:
* 	Can connect to 2 sensors sensors.
* 	Saves the data received from the sensors in the mongo database collections specified in the config files.


# Usage instructions:

## STEP1: get the application.
* Method 1 update existing repo: 

navigate to the folder with the application and run the script `update.sh`. 

You will then be asked to enter your gitlab ctredentials.

* Method 2 clone the repo: 

open the terminal (`Ctrl+Alt+t`) 

execute: `git clone https://gitlab.com/hddia/data_collector_release.git`

* Method 3 download the aplication from gitlab: 

Using a browser navigate to https://gitlab.com/hddia/data_collector_release 

click on download button

NOTE:  if you want the debug version replace the paths above with the following: https://gitlab.com/hddia/data_collector_release/tree/debug

## STEP2: if needed update the configuration files.

The configuration files are: "configuration_left_sensor.json" and "configuration_right_sensor.json"

For each file valid data for the following fields must be set:
* uri - address of the a running mongo server; the adress format should be: mongodb://address:port_number/
* name - name of the mongo database that will store the data
* collection - name of the collection for wich the data is saved. 
It will be prefered to define the collections with the following format: **patient_name_sex(M/F)_age_senzorSide**
* side - left/right acording to where the sensor is placed on the patient
* name - Bluetooth network name of the sensor
* address - 6 decimal value indicating the bluetooth network address of the sensor. Values will be placed between brackets and separated by comma
* timeout - the maximum time the sensor is allowed to send the alive message after the connection has been established

## Optional: start the mongo server

If you are running a local instance of the mongo server you should be able to start it by:
* open the default console by pressing: `Ctrl+Alt+t`
* start the mongo service with the command `sudo service mongod start`

NOTE: please make sure that the database and collections defined in the configuration files are defined in the database

## Optional: view the data saved in mongo with Compass

You can install Mongo Compass from here: https://www.mongodb.com/products/compass

After that you can create/update/delete mongo databases, collections and documents unsing the Compass application

## Step3: Run the application.

### Method1:
Navigate to the folder with the aplication and run the script `run.sh`

### Method2:
* You shuold be able to open the default terminal by pressing: `Ctrl+Alt+t`
* Then navgate to the directory where you downloaded the application. `cd ~/data_collector_release`
* Then run the application with `./data_collector`
* The application will display messages that indicate what it is dowing at this moment. The build from the debug branch will be more verberose



