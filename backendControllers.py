#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from subprocess import Popen, PIPE
from psutil import Process as ProcessMonitor
from threading import Thread

class DataCollectionHandler():
    def __init__(self):
        self.dataCollector = None
        self.dataCollectionThread = None
        self.prepareToEnd = False

    def isRunning(self):
        if self.dataCollectionThread is None:
            return False
        return self.dataCollectionThread.is_alive()

    def run(self, display):
        if self.isRunning():
            display('data collection process is already running')
        else:
            self.prepareToEnd = False
            self.dataCollectionThread = Thread(target=self.handleDataCollectionProcess, args=(display,))
            self.dataCollectionThread.start()

    def handleDataCollectionProcess(self, display):
        self.dataCollector = Popen(["./data_collector"], stdout=PIPE)
        dataCollectorProcess = ProcessMonitor(self.dataCollector.pid)
        while dataCollectorProcess.is_running():
            output = self.dataCollector.stdout.readline().rstrip()
            try:
                message = output.decode("utf-8")
            except UnicodeError or UnicodeDecodeError:
                message = output

            if message == '' or message is None or not message:
                break
            elif self.prepareToEnd:
                break
            else:
                display(message)

    def end(self):
        self.prepareToEnd = True
        if self.dataCollectionThread is not None:
            self.dataCollector.kill()
            self.dataCollector.wait()
        if self.isRunning():
            self.dataCollectionThread.join()

