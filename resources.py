#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

LEFT_SENSOR_DATA_FILE = "configuration_left_sensor.json"
LEFT_SENSOR_LABEL = "left"

RIGHT_SENSOR_DATA_FILE = "configuration_right_sensor.json"
RIGHT_SENSOR_LABEL = "right"

BACKGROUND_COLOR = 'white'

MAX_NUMBER_OF_MESSAGES = 100.0

MAX_AGE_YEARS = 200

DATABASE_JSON_KEY = 'database'
SENSOR_JSON_KEY = 'sensor'
COLLECTION_FIELD_DELIMITER = '_'

genderOptions = ['M', 'F']