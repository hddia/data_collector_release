#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from tkinter import ttk, Frame, StringVar, Label, Entry, TRUE, X

from resources import BACKGROUND_COLOR, genderOptions
from validators import removeInvalidYearsCharacter, removeInvalidMonthCharacter, removeInvalidGenderOptions, isValidAgeYears, isValidMonthValue, isValidGenderOption

class Database():
    def __init__(self, parent, padding):
        self.frame = Frame(parent, bg=BACKGROUND_COLOR)
        self.frame.grid_columnconfigure(1, weight=1)

        self.address = StringVar()
        self.addEntryBox(self.frame, 'Database server: ', self.address, padding.fieldYpadding, 0, 0)

        self.name = StringVar()
        self.addEntryBox(self.frame, 'Database name: ', self.name, padding.fieldYpadding, 1, 0)

        self.patientName = StringVar()
        self.addEntryBox(self.frame, 'Patient name: ', self.patientName, padding.fieldYpadding, 2, 0)

        self.ageYears = StringVar()
        self.ageYears.trace('w', lambda *_, var=self.ageYears: removeInvalidYearsCharacter(var))
        self.addEntryBox(self.frame, 'Patient age in years: ', self.ageYears, padding.fieldYpadding, 3, 0)

        self.ageMonths = StringVar()
        self.ageMonths.trace('w', lambda *_, var=self.ageMonths: removeInvalidMonthCharacter(var))
        self.addEntryBox(self.frame, 'Patient age in months: ', self.ageMonths, padding.fieldYpadding, 4, 0)

        self.gender = StringVar()
        self.gender.trace('w', lambda *_, var=self.gender: removeInvalidGenderOptions(var))
        self.addGenderSelector(self.frame, 'Gender: ', self.gender, padding.fieldYpadding, 5, 0)

        self.collectionFieldDelimiter = '_'

    def addGenderSelector(self, parent, label, var, yPadding, baseRow, baseCollumn):
        label = Label(parent, text=label, bg=BACKGROUND_COLOR)
        label.grid(row=baseRow, column=baseCollumn, sticky='w', pady=yPadding)
        entry = ttk.Combobox(parent, textvariable=var, width=5, justify='center', values=genderOptions)
        entry.grid(row=baseRow, column=baseCollumn+1, sticky='w', pady=yPadding)

    def addEntryBox(self, parent, label, var, yPadding, baseRow, baseCollumn):
        label = Label(parent, text=label, bg=BACKGROUND_COLOR)
        label.grid(row=baseRow, column=baseCollumn, sticky='w', pady=yPadding)
        entry = Entry(parent, textvariable=var, bg=BACKGROUND_COLOR)
        entry.grid(row=baseRow, column=baseCollumn+1, sticky='we', pady=yPadding)

    def pack(self, padding):
        yPadding = (padding.containerLeftPadRatio*padding.fieldYpadding, padding.containerRightPadRation*padding.fieldYpadding)
        xPadding = (padding.containerLeftPadRatio*padding.fieldYpadding, padding.containerRightPadRation*padding.fieldYpadding)
        self.frame.pack(pady=yPadding, padx=xPadding, anchor='e', expand=TRUE, fill=X)

    def getAddress(self):
        return self.address.get()

    def getName(self):
        return self.name.get()

    def getPatientName(self):
        return self.patientName.get()

    def setPatientName(self, value):
        self.patientName.set(value)

    def getAgeYears(self):
        return self.ageYears.get()

    def setAgeYears(self, value):
        if isValidAgeYears(value):
            self.ageYears.set(value)

    def getAgeMonths(self):
        return self.ageMonths.get()

    def setAgeMonths(self, value):
        if isValidMonthValue(value):
            self.ageMonths.set(value)

    def getGender(self):
        return self.gender.get()

    def setGender(self, value):
        if isValidGenderOption(value):
            self.gender.set(value)

    def setAddress(self, address):
        self.address.set(address)

    def setName(self, name):
        self.name.set(name)

    def setCollection(self, collection):
        components = collection.split(self.collectionFieldDelimiter)
        print(components)

    def validateInputFields(self):
        address = self.getAddress()
        if address is '':
            return 'Invalid database address!'
        if self.getName() is '':
            return 'Invalid database name!'
        if self.getPatientName() is '':
            return 'Invalid patient name!'
        if self.getAgeYears() is '':
            return 'Invalid patient age in years!'
        if self.getAgeMonths() is '':
            return 'Invalid patient age in months!'
        if self.getGender() is '':
            return 'Invalid patient gender!'
        return None
