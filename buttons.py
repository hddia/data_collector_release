#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from tkinter import Button, LEFT, DISABLED, NORMAL

from resources import BACKGROUND_COLOR

class Buttons():
    def __init__(self, parentContainer, save, start, stop):
        self.saveConfigurationButton = Button(parentContainer, text='Save configuration', bg=BACKGROUND_COLOR, command=save)
        self.startButton = Button(parentContainer, text='Start data collection', bg=BACKGROUND_COLOR, command=start)
        self.stopButton = Button(parentContainer, text='Stop data collection', bg=BACKGROUND_COLOR, command=stop)

    def pack(self, padding):
        self.saveConfigurationButton.pack(side=LEFT, pady=padding.fieldYpadding, padx=padding.fieldXpadding, anchor='w')
        self.startButton.pack(side=LEFT, pady=padding.fieldYpadding, padx=padding.fieldXpadding, anchor='w')
        self.stopButton.pack(side=LEFT, pady=padding.fieldYpadding, padx=padding.fieldXpadding, anchor='w')
        self.setInInitialState()

    def setInProcessingState(self):
        self.saveConfigurationButton.config(state=DISABLED)
        self.startButton.config(state=DISABLED)
        self.stopButton.config(state=DISABLED)

    def setInInitialState(self):
        self.saveConfigurationButton.config(state=NORMAL)
        self.startButton.config(state=NORMAL)
        self.stopButton.config(state=DISABLED)

    def setInDataAquisitionRunningState(self):
        self.saveConfigurationButton.config(state=DISABLED)
        self.startButton.config(state=DISABLED)
        self.stopButton.config(state=NORMAL)